"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const seminal_web3_service_1 = require("seminal-web3-service");
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
router.post('/transferCatalogueCollection', async (req, res, next) => {
    const { value, catalogueAddress, fromAddress, toAddress, saleOfIpUrl, certificateUrl, } = req.body;
    return await seminal_web3_service_1.SeminalCollectionService.requestTransferCatalogueCollection(value, catalogueAddress, fromAddress, toAddress, saleOfIpUrl, certificateUrl);
});
router.post('/deleteCatalogueCollection', async (req, res, next) => {
    const { catalogueAddress, saleOfIpUrl, certificateUrl, } = req.body;
    return await seminal_web3_service_1.SeminalCollectionService.requestDeleteCatalogueCollection(catalogueAddress, saleOfIpUrl, certificateUrl);
});
exports.default = router;
