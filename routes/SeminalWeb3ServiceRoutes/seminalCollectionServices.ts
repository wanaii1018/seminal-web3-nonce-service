import {NextFunction, Request, Response} from 'express';
import {SeminalCollectionService} from 'seminal-web3-service';

const express = require('express');


// eslint-disable-next-line new-cap
const router = express.Router();

router.post('/transferCatalogueCollection', async (req: Request, res: Response, next: NextFunction) => {
  const {
    value,
    catalogueAddress,
    fromAddress,
    toAddress,
    saleOfIpUrl,
    certificateUrl,
  } = req.body;

  return await SeminalCollectionService.requestTransferCatalogueCollection(
    value,
    catalogueAddress,
    fromAddress,
    toAddress,
    saleOfIpUrl,
    certificateUrl,
  );
});

router.post('/deleteCatalogueCollection', async (req: Request, res: Response, next: NextFunction) => {
  const {
    catalogueAddress,
    saleOfIpUrl,
    certificateUrl,
  } = req.body;

  return await SeminalCollectionService.requestDeleteCatalogueCollection(
    catalogueAddress,
    saleOfIpUrl,
    certificateUrl,
  );
});

export default router;
