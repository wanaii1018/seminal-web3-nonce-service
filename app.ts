import express, {Express, NextFunction, Request, Response} from 'express';
// @ts-ignore
import createError from 'http-errors';
import path from 'path';
// @ts-ignore
import cookieParser from 'cookie-parser';
// @ts-ignore
import logger from 'morgan';
// @ts-ignore
import cors from 'cors';

// @ts-ignore
import nonceController from './routes/SeminalWeb3ServiceRoutes/nonceController';
// @ts-ignore
import collectionController from './routes/SeminalWeb3ServiceRoutes/seminalCollectionServices';

const app: Express = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/seminalWeb3Service/nonce', nonceController);
app.use('/seminalWeb3Service/collection', collectionController);

// catch 404 and forward to error handler
app.use((req: Request, res: Response, next: NextFunction) => {
  next(createError(404));
});

// error handler
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
